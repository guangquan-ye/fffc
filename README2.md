J'ai commencé par crée un fichier "customers.txt" dans lequel j'ai crée plusieurs customers suivant l'exemple "1970-01-01John Smith 81.5", puis crée le "metadata.csv" pour integré la meta-donnée.

1. Lecture metadata.csv

je fais une fonction convertFixedFormattoCsv($dataFile, $metadataFile, $outputFileBase) qui prend en param : 

$dataFile = 'customers.txt';
$metadataFile = 'metadata.csv';
$outputFileBase = 'customers';

j'ouvre le metadata.csv avec la fonction fopen() en mode "read", et j'assigne le pointeur du fichier à $handle. 

* le pointeur est une variable qui représente le fichier ouvert, et il est utilisé pour effectuer des opérations sur ce fichier, comme l'ouverture, la lecture ou la fermeture.

je crée un tableau metadata[] vide qui me permettra de stocker les metadata. 

je set une condition si la fonction n'arrive pas a ouvrir le fichier il s'arrete et s'il reussit alors la boucle while parcourt $metadataFile avec $row=fgetcsv() pour lire une ligne du fichier de metadata.csv et la sépare en valeurs à l'aide du déliminiteur (","), et la stock dans $row. la boucle continue tant que fgetcsv() renvoie une ligne valide.

je stock ensuite $row à la fin de $metadata[]. Ainsi, à chaque tour de boucle une ligne est ajoutée à metadata[].

Et je ferme le fichier avec fclose().

2. Ecriture de customers.csv 

j'ouvre customers.csv toujours avec fopen() mais en ecriture cette fois ("w") avec le pointeur $file. 

$columnNames = array_map('trimColumnValues', array_column($metadata, 0)); 

* la fonction trimColumnValues() defini plus bas dans le code est utilisée pour supprimer les espaces et les virgules aves les funtions str_replace() et trim()

la fonction array_column() me permet d'extraire la première colonne de $metadata[], ensuite j'utilise array_map pour appliquer la fonction trimColumnValues() à chaque élément de la première colonne pour supprimer les espaces et virgules. 

le resultat est stocké dans $columnNames.

J'ecris dans $customers.csv avec la fonction fwrite($customers.csv, implode(',', $columnNames) . "\n");

* implode() sert à concaténer les éléments du tableau $columnNames en une chaîne de caractères, en les séparant par une virgule (','). Cela crée une seule chaîne qui contient tous les noms de colonne séparés par des virgules.

* "\n" : Cela ajoute un caractère de saut de ligne à la fin de la chaîne. 

je crée une boucle while une fois de plus pour lire chaque ligne du fichier customers.txt avec fgets(), et stock la valeurs des lignes lues dans $row 

et toujours la boucle while va effectuer différentes manipulations sur chaque ligne lue du fichier : 

- Crée un tableau vide $row pour stocker les valeurs de chaque ligne.

- Fais une boucle foreach pour parcourir les métadonnées du tableau $metadata[].

- Pour chaque colonne, il extrait une partie de la ligne en fonction de la longueur spécifiée dans les métadonnées et la stocke dans la variable $value.
Selon le type de colonne spécifié dans les métadonnées, il effectue des transformations supplémentaires sur la valeur (format date, extraction de noms, conversion en nombre flottant, etc.).

- La valeur traitée est ajoutée au tableau $row, et la variable $start est mise à jour pour suivre la position de départ de la prochaine colonne dans la ligne.

Je filtre avec $nonEmptyValues = array_filter($row, function ($value) { return !empty($value); }); pour filtrer les valeurs de $row et garde que celles non vides.

J'écris avec fwrite($file, implode(',', $nonEmptyValues) . "\n"); dans $file les valeurs de $nonEmptyValues.

et je fclose $file.

* les fonctions 

function reformatDate($date) {
    $parts = explode('-', $date);
    return $parts[2] . '/' . $parts[1] . '/' . $parts[0];
}

j'ai fais une fonction pour mettre la date en format demandé . 

* Les fichier et conditions 

j'ai crée aussi une condition pour lire que les fichiers.txt 

if (pathinfo($dataFile, PATHINFO_EXTENSION) === 'txt') {
    convertFixedFormatToCsv($dataFile, $metadataFile, $outputFile);
} else {
    echo "Erreur, il faut un fichier .txt";
}

et pour finir je fais une boucle while pour accrémenter de (1) si le fichier crée existe deja.

$outputFile = $outputFileBase . $fileExtension;
while (file_exists($outputFile)) {
    $fileCounter++;
    $outputFile = $outputFileBase . '(' . $fileCounter . ')' . $fileExtension;
}