<?php

$dataFile = 'customers.txt';
// $dataFile = 'customers.php';
$metadataFile = 'metadata.csv';
$outputFileBase = 'customers';
$fileCounter = 0;
$fileExtension = '.csv';

$outputFile = $outputFileBase . $fileExtension;
while (file_exists($outputFile)) {
    $fileCounter++;
    $outputFile = $outputFileBase . '(' . $fileCounter . ')' . $fileExtension;
}

if (pathinfo($dataFile, PATHINFO_EXTENSION) === 'txt') {
    convertFixedFormatToCsv($dataFile, $metadataFile, $outputFile);
} else {
    echo "Erreur, il faut un fichier .txt";
}

function convertFixedFormatToCsv($dataFile, $metadataFile, $outputFile) {
    $metadata = [];
    if (($handle = fopen($metadataFile, "r")) !== FALSE) {
        while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $metadata[] = $row;
        }
        fclose($handle);
    }

    $file = fopen($outputFile, 'w');

    $columnNames = array_map('trimColumnValues', array_column($metadata, 0));
    fwrite($file, implode(',', $columnNames) . "\n");

    if (($handle = fopen($dataFile, "r")) !== FALSE) {
        while (($line = fgets($handle)) !== FALSE) {
            $row = [];
            $start = 0;
            foreach ($metadata as $column) {
                $length = intval($column[1]);
                $value = trim(substr($line, $start, $length));
            
                if ($column[2] == 'date') {
                    $value = reformatDate($value);
                } elseif ($column[0] == 'Nom de famille') {
                    $names = explode(' ', $line);
                    $value = $names[count($names) - 2];
                } elseif ($column[0] == 'Prénom') {
                    $names = explode(' ', $line);
                    $value = $names[count($names) - 3];
                } elseif ($column[0] == 'Poids') {
                    $parts = explode(' ', $line);
                    $value = floatval($parts[count($parts) - 1]);
                } elseif ($column[2] == 'numérique') {
                    $value = floatval($value);
                }
            
                $row[] = $value;
                $start += $length;
            }
            
            $nonEmptyValues = array_filter($row, function ($value) {
                return !empty($value);
            });
            fwrite($file, implode(',', $nonEmptyValues) . "\n");
        }
        fclose($handle);
    }

    fclose($file);
}

function trimColumnValues($value) {
    return str_replace(',', '', trim($value));
}

function reformatDate($date) {
    $parts = explode('-', $date);
    return $parts[2] . '/' . $parts[1] . '/' . $parts[0];
}
